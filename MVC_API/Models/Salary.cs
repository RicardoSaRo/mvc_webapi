﻿using System.ComponentModel.DataAnnotations;

namespace MVC_API.Models
{
    public class Salary
    {
        public int SalaryID { get; set; }

        [Required]
        public int Amount { get; set; }
    }
}
