﻿using System.ComponentModel.DataAnnotations;

namespace MVC_API.Models
{
    public class Department
    {
        public int DepartmentID { get; set; }

        [Required (ErrorMessage ="Please fill department Name Area")]
        public string DepartmentName { get; set; }
    }
}
