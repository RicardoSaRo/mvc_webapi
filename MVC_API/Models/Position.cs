﻿using System.ComponentModel.DataAnnotations;

namespace MVC_API.Models
{
    public class Position
    {
        public int PositionID { get; set; }

        [Required]
        public string PositionName { get; set; }

        public int DepartmentID { get; set; }

        public Department Department { get; set; }
    }
}
