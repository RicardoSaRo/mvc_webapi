﻿namespace MVC_API.Models
{
    public class File
    {
        public int FileID { get; set; }

        public int PersonID { get; set; }

        public string ImagePath { get; set; }


    }
}
