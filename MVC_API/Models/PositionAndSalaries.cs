﻿namespace MVC_API.Models
{
    public class PositionAndSalaries
    {
        public List<Position> PositionList { get; set; }

        public List<Salary> SalaryList { get; set; }
    }
}
