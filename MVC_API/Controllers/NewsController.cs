﻿using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace MVC_API.Controllers
{
    public class NewsController : Controller
    {
        //News API Key: f418ee1f8ec34820ba656755dc8babcf
        [HttpGet]
        public async Task<IActionResult> News()
        {
            string newsUri = "https://newsapi.org/v2/everything?q=apple&from=2022-09-17&to=2022-09-17&sortBy=popularity&apiKey=f418ee1f8ec34820ba656755dc8babcf";
            Root root = new Root();
            HttpClient client = new HttpClient();
            HttpResponseMessage responseMessage = await client.GetAsync(newsUri);
            if (responseMessage.IsSuccessStatusCode)
            {
                var jstring = await responseMessage.Content.ReadAsStringAsync();
                root = JsonConvert.DeserializeObject<Root>(jstring);
            }
            return View(root);

        }
    }
}
