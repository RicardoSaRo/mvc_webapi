﻿using Microsoft.AspNetCore.Mvc;
using MVC_API.Models;
using Newtonsoft.Json;
using System.Text;

namespace MVC_API.Controllers
{
    public class PersonController : Controller
    {
        public async Task<IActionResult> Index()
        {
            HttpClient client = new HttpClient();
            HttpResponseMessage responseMessage = await client.GetAsync("http://localhost:49392/api/persons");
            if (responseMessage.IsSuccessStatusCode)
            {
                var jstring = await responseMessage.Content.ReadAsStringAsync();
                List<PersonALL> list = JsonConvert.DeserializeObject<List<PersonALL>>(jstring);
                return View(list);
            }
            else
                return View(new List<PersonALL>());
        }

        public async Task<IActionResult> Add()
        {
            //--> Consume API for Positions and Salaries and fill the view selects
            Person person = new Person();
            HttpClient client = new HttpClient();
            HttpResponseMessage responseMessage = await client.GetAsync("http://localhost:49392/api/persons/positionsandsalaries");
            if (responseMessage.IsSuccessStatusCode)
            {
                var jstring = await responseMessage.Content.ReadAsStringAsync();
                PositionAndSalaries PaS = JsonConvert.DeserializeObject<PositionAndSalaries>(jstring);
                ViewBag.PositionList = PaS.PositionList;
                ViewBag.SalaryList = PaS.SalaryList;
                //--> Added the lists to ViewBag for view usage
            }
            return View(person);
        }
        [HttpPost]
        public async Task<IActionResult> Add(Person model)
        {
            if (ModelState.IsValid)
            {
                HttpClient client = new HttpClient();
                var jsonperson = JsonConvert.SerializeObject(model);
                StringContent content = new StringContent(jsonperson, Encoding.UTF8, "application/json");
                HttpResponseMessage responseMessage = await client.PostAsync("http://localhost:49392/api/persons", content);
                if (responseMessage.IsSuccessStatusCode)
                {
                    return RedirectToAction("Index");
                }
                else
                {
                    ModelState.AddModelError("Error", "There is an API error");
                    return View(model);
                }
            }
            else
            {
                return View(model);
            }
        }

        public async Task<IActionResult> UpdatePerson(int Id)
        {
            //--> Consume API for Positions and Salaries and fill the view selects
            HttpClient client = new HttpClient();
            HttpResponseMessage responseMessage = await client.GetAsync("http://localhost:49392/api/persons/positionsandsalaries");
            if (responseMessage.IsSuccessStatusCode)
            {
                var jstring = await responseMessage.Content.ReadAsStringAsync();
                PositionAndSalaries PaS = JsonConvert.DeserializeObject<PositionAndSalaries>(jstring);
                ViewBag.PositionList = PaS.PositionList;
                ViewBag.SalaryList = PaS.SalaryList;
            }
            else
            {
                PositionAndSalaries PaS = new PositionAndSalaries();
                ViewBag.PositionList = PaS.PositionList;
                ViewBag.SalaryList = PaS.SalaryList;
            }
            //--> Consume API for the actual person to Update
            client = new HttpClient();
            responseMessage = await client.GetAsync("http://localhost:49392/api/persons/" + Id);
            if (responseMessage.IsSuccessStatusCode)
            {
                var jstring = await responseMessage.Content.ReadAsStringAsync();
                Person person = JsonConvert.DeserializeObject<Person>(jstring);
                return View(person);
            }
            else
                return RedirectToAction("Index");
        }
        [HttpPost]
        public async Task<IActionResult> UpdatePerson(Person model)
        {
            if (ModelState.IsValid)
            {
                HttpClient client = new HttpClient();
                var jsonperson = JsonConvert.SerializeObject(model);
                StringContent content = new StringContent(jsonperson, Encoding.UTF8, "application/json");
                HttpResponseMessage responseMessage = await client.PutAsync("http://localhost:49392/api/persons", content);
                if (responseMessage.IsSuccessStatusCode)
                {
                    return RedirectToAction("Index");
                }
                else
                {
                    ModelState.AddModelError("Error", "There is an API error");
                    return View(model);
                }
            }
            else
            {
                return View(model);
            }
            
        }

        public IActionResult UploadFile()
        {
            return View();
        }
        [HttpPost]
        public async Task<IActionResult> UploadFile([FromForm]IFormFile file)
        {
            //--> To upload the file in the API the client MUST save the file (even if temp)
            //--> Therefore is necesary to make an Async Copy of the file before making it into
            //--> a byte array (if not, the client will be unable to find the temp file to send
            //--> to the API. I am using a "try" mostly to be sure that the temp file is deleted
            //--> in "finally". Sorted the code for the "try" and "finally" to use their vars)
            var filePath = Path.GetTempFileName();
            MultipartFormDataContent formData = new MultipartFormDataContent();
            using (var stream = System.IO.File.Create(filePath))
            {
                await file.CopyToAsync(stream);
            }
            try
            {
                var bytes = await System.IO.File.ReadAllBytesAsync(filePath);
                ByteArrayContent content = new ByteArrayContent(bytes);
                formData.Add(content, "file", file.FileName);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (System.IO.File.Exists(filePath)) 
                    System.IO.File.Delete(filePath);
            }
            HttpClient client = new HttpClient();
            HttpResponseMessage responseMessage = await client.PostAsync("http://localhost:49392/api/persons/uploadfile", formData);
            if (responseMessage.IsSuccessStatusCode)
            {
                return RedirectToAction("Index");
            }
            else
                return View();
        }
    }
}
