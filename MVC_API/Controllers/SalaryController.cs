﻿using Microsoft.AspNetCore.Mvc;
using MVC_API.Models;
using Newtonsoft.Json;
using System.Text;

namespace MVC_API.Controllers
{
    public class SalaryController : Controller
    {
        public async Task<IActionResult> Index()
        {
            HttpClient client = new HttpClient();
            HttpResponseMessage responseMessage = await client.GetAsync("http://localhost:49392/api/salaries");
            if (responseMessage.IsSuccessStatusCode)
            {
                List<Salary> list = new List<Salary>();
                var jstring = await responseMessage.Content.ReadAsStringAsync();
                list = JsonConvert.DeserializeObject<List<Salary>>(jstring);
                return View(list);
            }
            else
                return View(new List<Salary>());
        }

        public IActionResult Add()
        {
            Salary salary = new Salary();
            return View(salary);
        }
        [HttpPost]
        public async Task<IActionResult> Add(Salary model)
        {
            HttpClient client = new HttpClient();
            var jsonsalary = JsonConvert.SerializeObject(model);
            StringContent content = new StringContent(jsonsalary, Encoding.UTF8, "application/json");
            HttpResponseMessage responseMessage = await client.PostAsync("http://localhost:49392/api/salaries", content);
            if (responseMessage.IsSuccessStatusCode)
            {
                return RedirectToAction("Index");
            }
            else
                return View(model);
        }

        public async Task<IActionResult> DeleteSalary(int Id)
        {
            HttpClient client = new HttpClient();
            HttpResponseMessage responseMessage = await client.DeleteAsync("http://localhost:49392/api/salaries/" + Id);
            if (responseMessage.IsSuccessStatusCode)
            {
                return RedirectToAction("Index");
            }
            else
                return RedirectToAction("Add");
        }
    }
}
